import os
import Item


def get_settings():
    settings_file = open(os.path.dirname(os.path.abspath(__file__)) + '/settings.txt')
    settings_dict = {}
    for line in settings_file.readlines():
        settings_dict[line.split('=')[0]] = line.split('=')[1].replace('\n', '')  # поскольку каждый параметр записан
        # в новой строке, в результат попадает символ переноса, который необходимо убрать
    return settings_dict


print(u'Добро пожаловать!\nВнимание!\nДля корректной работы программы '
      u'укажите нужные настройки, в том числе путь сохранения, в файле "settings.txt"')

url = input(u'Введите url: ')

item = Item.Item(url, get_settings())

try:
    item.write_to_file()
except:
    print(u'Произошла ошибка в работе программы. Программа будет закрыта.')
