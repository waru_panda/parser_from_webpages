# coding:utf-8
from bs4 import BeautifulSoup
import os
import urllib.request


class Item(object):
    def __init__(self, url, settings):
        self.url = url
        self.settings = settings

    def get_text(self):
        response = urllib.request.urlopen(self.url)
        soup = BeautifulSoup(response, 'lxml')

        first_header = soup.find('h1')
        paragraphs_list = first_header.find_all_next(['h1', 'h2', 'p', 'ul'])

        for paragraph in paragraphs_list:
            links_list = paragraph.find_all('a')
            for link in links_list:
                link_href = link.get('href')
                link.insert(1, '[{}]'.format(link_href))

            if paragraph.find_all_next('p') is not []:
                li_list = paragraph.find_all('li')
                for li in li_list:
                    li.insert(0, '>')

        # результат вывода с некоторых сайтов содержит '\xa0' из-за отличающейся кодировки
        paragraphs_text_with_links = [first_header.text.replace(u'\xa0', u' ')] + [paragraph.text.replace(u'\xa0', u' ')
                                                                                   for paragraph in paragraphs_list]
        return paragraphs_text_with_links

    def create_file(self):
        # если url оканчивается на /, то последним элементом явлется None, и файл не создается
        if self.url.endswith(u'/'):
            url = self.url[:-1].split('/')[2:]
        else:
            url = self.url.split('/')[2:]

        file_name = url.pop(len(url) - 1) + '.txt'
        path = self.settings.get('path') + '/' + '/'.join(url) + '/'

        try:
            if not os.path.exists(path):
                os.makedirs(path)
            return path + file_name
        except ValueError:
            print(u'В настройках указан некорректный путь сохранения файла')

    def format_text(self):
        list_for_format = self.get_text()
        ready_text = ''
        try:
            for paragraph in list_for_format:
                paragraph_to_words_list = paragraph.split(' ')

                i = 0
                formatted = []
                for word in paragraph_to_words_list:
                    i += len(word)
                    if i > int(self.settings.get('string_length', None)):
                        i = 0
                        word = '\n' + word
                        formatted.append(word)
                    else:
                        formatted.append(word)
                formatted_paragraph = ' '.join(formatted) + '\n\n'
                ready_text += formatted_paragraph
            return ready_text
        except ValueError:
            print(u'В настройках задана неккоректная длина строки.')

    def write_to_file(self):
        file = open(self.create_file(), 'w+')
        text_to_write = self.format_text()
        file.write(text_to_write)
